# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from treinamento.portlets.testing import TREINAMENTO_PORTLETS_INTEGRATION_TESTING  # noqa
from plone import api

import unittest


class TestSetup(unittest.TestCase):
    """Test that treinamento.portlets is properly installed."""

    layer = TREINAMENTO_PORTLETS_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if treinamento.portlets is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'treinamento.portlets'))

    def test_browserlayer(self):
        """Test that ITreinamentoPortletsLayer is registered."""
        from treinamento.portlets.interfaces import (
            ITreinamentoPortletsLayer)
        from plone.browserlayer import utils
        self.assertIn(ITreinamentoPortletsLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = TREINAMENTO_PORTLETS_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['treinamento.portlets'])

    def test_product_uninstalled(self):
        """Test if treinamento.portlets is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'treinamento.portlets'))

    def test_browserlayer_removed(self):
        """Test that ITreinamentoPortletsLayer is removed."""
        from treinamento.portlets.interfaces import ITreinamentoPortletsLayer
        from plone.browserlayer import utils
        self.assertNotIn(ITreinamentoPortletsLayer, utils.registered_layers())
