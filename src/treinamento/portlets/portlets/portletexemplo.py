from zope.interface import implements

from plone.portlets.interfaces import IPortletDataProvider
from plone.app.portlets.portlets import base

from zope.formlib import form
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class IPortletExemplo(IPortletDataProvider):
    """ """

class Assignment(base.Assignment):
    implements(IPortletExemplo)

    def __init__(self):
        pass

    @property
    def title(self):
        return "Portlet Exemplo"

class Renderer(base.Renderer):
    render = ViewPageTemplateFile('portletexemplo.pt')


class AddForm(base.AddForm):
    form_fields = form.Fields(IPortletExemplo)

    def create(self, data):
        return Assignment(**data)

class EditForm(base.EditForm):
    form_fields = form.Fields(IPortletExemplo)
